// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('app', ['ionic']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
});

//configura as rotas ou estados da aplicacao
app.config(function($stateProvider,$urlRouterProvider){

  $stateProvider
  .state('cadastro',{
    url: '/cadastro',
    templateUrl: 'cadastro.html',
    controller: 'ctrl'
  })
  .state('listar',{
    url: '/listar',
    templateUrl: 'listar.html',
    controller: 'ctrl'
  });
  $urlRouterProvider.otherwise('/cadastro');
});

//controller usa o $scope para os dados - MVC
app.controller('ctrl',function($scope){

   //Isso é um objeto javascript - class
   //O Ionic passa objetos no $scope
   $scope.data = {};

   $scope.salvar = function(){
      window.localStorage.setItem($scope.data.nome,$scope.data.nome);
   }

   $scope.carregar = function(){
      keys = Object.keys(window.localStorage);

      $scope.nomes = [];
      
      for (var i=0; i < window.localStorage.length; i++){
        $scope.nomes.push(window.localStorage.getItem(keys[i]));
      }
   }

   $scope.carregar();
});
